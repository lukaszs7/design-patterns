package com.szmolke.patterns.creational.builder;

/**
 * @author lszmolke
 */
public class Test {
    public static void main(String[] args) {
        //Using builder to get the object in a single line of code and
        //without any inconsistent state or arguments management issues
        Computer comp = new Computer.ComputerBuilder("500 GB", "2 GB")
                .withBluetoothEnabled(true)
                .withGraphicsCardEnabled(true)
                .build();
        System.out.println(comp);
    }
}

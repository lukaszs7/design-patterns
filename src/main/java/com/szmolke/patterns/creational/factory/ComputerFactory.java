package com.szmolke.patterns.creational.factory;

/**
 * @author lszmolke
 */
public class ComputerFactory {

    public static Computer getComputer(String type, String ram, String hdd, String cpu) {
        if ("PC".equalsIgnoreCase(type)) return new PC(ram, hdd, cpu);
        else if ("Server".equalsIgnoreCase(type)) return new Server(ram, hdd, cpu);

        throw new IllegalArgumentException("Cannot find Computer implementation");
    }
}
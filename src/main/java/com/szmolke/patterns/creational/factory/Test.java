package com.szmolke.patterns.creational.factory;

/**
 * @author lszmolke
 */
public class Test {
    public static void main(String[] args) {
        Computer pc = ComputerFactory.getComputer("pc","2 GB","500 GB","2.4 GHz");
        Computer server = ComputerFactory.getComputer("server","16 GB","1 TB","2.9 GHz");
        System.out.println("Factory PC Config::"+pc+". Type: "+pc.getClass().getSimpleName());
        System.out.println("Factory Server Config::"+server+". Type: "+server.getClass().getSimpleName());
    }
}

package com.szmolke.patterns.creational.abstractfactory;

/**
 * @author lszmolke
 */
public interface ComputerAbstractFactory {
    Computer createComputer();
}

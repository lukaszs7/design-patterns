package com.szmolke.patterns.creational.abstractfactory;

/**
 * @author lszmolke
 */
public class ComputerFactory {

    public static Computer getComputer(ComputerAbstractFactory factory) {
        return factory.createComputer();
    }
}
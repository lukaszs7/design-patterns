/**
 Abstract Factory pattern is similar to Factory pattern and it’s factory of factories.
 If you are familiar with factory design pattern in java, you will notice that we have a single Factory class that returns
 the different sub-classes based on the input provided and factory class uses if-else or switch statement to achieve this.
 *
 * @see resource uml.creational "abstractfactory.PNG"
 * @author lszmolke
 */
package com.szmolke.patterns.creational.abstractfactory;
package com.szmolke.patterns.creational.singleton;

/**
 * Advantages:
 * - Thread safe
 * - easy
 * Disadvantages:
 * - When singleton uses a lot of resources, cost will be large (Database connection, Clients) etc.
 * - This method doesn’t provide any options for exception handling.
 *
 * @author lszmolke
 */
public class EagerInitialization {
    private static final EagerInitialization instance = new EagerInitialization();

    private EagerInitialization() {}

    public EagerInitialization getInstance() {
        return instance;
    }
}

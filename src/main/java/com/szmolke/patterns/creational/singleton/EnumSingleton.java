package com.szmolke.patterns.creational.singleton;

/**
 * Advantages:
 * - Easy
 * - Thread safe
 * Disadvantages:
 * - Eager loading
 * @author lszmolke
 */
public enum EnumSingleton {
    INSTANCE;
}

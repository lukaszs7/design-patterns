package com.szmolke.patterns.creational.singleton;

/**
 * Same as EagerInitialization, but we can handle exceptions.
 *
 * @author lszmolke
 */
public class EagerStaticBlockInitialization {

    private static EagerStaticBlockInitialization instance;

    private EagerStaticBlockInitialization(){}

    //static block initialization for exception handling
    static{
        try{
            instance = new EagerStaticBlockInitialization();
        }catch(Exception e){
            throw new RuntimeException("Exception occured in creating singleton instance");
        }
    }

    public static EagerStaticBlockInitialization getInstance(){
        return instance;
    }
}

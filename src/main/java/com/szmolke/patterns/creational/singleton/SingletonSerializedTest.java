package com.szmolke.patterns.creational.singleton;

import java.io.*;

/**
 * Test serialization. Ensure that serialization do not destroy singletion pattern.
 * @author lszmolke
 */
public class SingletonSerializedTest {

    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        StaticInnerClassInitialization instanceOne = StaticInnerClassInitialization.getInstance();
        ObjectOutput out = new ObjectOutputStream(new FileOutputStream(
                "filename.ser"));
        out.writeObject(instanceOne);
        out.close();

        //deserailize from file to object
        ObjectInput in = new ObjectInputStream(new FileInputStream(
                "filename.ser"));
        StaticInnerClassInitialization instanceTwo = (StaticInnerClassInitialization) in.readObject();
        in.close();

        System.out.println("instanceOne hashCode="+instanceOne.hashCode());
        System.out.println("instanceTwo hashCode="+instanceTwo.hashCode());
    }
}

package com.szmolke.patterns.creational.singleton;

/**
 * Advantages:
 * - Easy
 * - Lazy loading
 * Disadvantages
 * - Not thread safe
 *
 * @author lszmolke
 */
public class LazyInitialization {
    private static LazyInitialization instance;

    private LazyInitialization(){}

    public static LazyInitialization getInstance(){
        if(instance == null){
            instance = new LazyInitialization();
        }
        return instance;
    }
}

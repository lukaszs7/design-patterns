package com.szmolke.patterns.creational.singleton;

/**
 * Same as LazyInitialization, but thread safe
 *
 * @author lszmolke
 */
public class ThreadSafeLazyInitialization {
    private static ThreadSafeLazyInitialization instance;

    private ThreadSafeLazyInitialization(){}


    /**
     * Optimalization problem
     * */
    public static synchronized ThreadSafeLazyInitialization getInstance(){
        if(instance == null){
            instance = new ThreadSafeLazyInitialization();
        }
        return instance;
    }


    /**
     * - Without optimalization problem
     * - Double checked locking
     * */
    public static ThreadSafeLazyInitialization getInstance2(){
        if(instance == null){
            synchronized (ThreadSafeLazyInitialization.class) {
                if(instance == null) {
                    instance = new ThreadSafeLazyInitialization();
                }
            }

        }
        return instance;
    }
}

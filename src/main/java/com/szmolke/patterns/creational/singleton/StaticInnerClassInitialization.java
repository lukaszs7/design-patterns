package com.szmolke.patterns.creational.singleton;

import java.io.Serializable;

/**
 * Advantages:
 * - Easy
 * - Lazy initialization. Inner class loaded after getInstance() call
 * - Thread safe
 * @author lszmolke
 */
public class StaticInnerClassInitialization implements Serializable {

    private static final long serialVersionUID = 42L;

    private StaticInnerClassInitialization(){}

    private static class SingletonHelper {
        private static final StaticInnerClassInitialization INSTANCE = new StaticInnerClassInitialization();
    }

    public static StaticInnerClassInitialization getInstance(){
        return SingletonHelper.INSTANCE;
    }

    /**
     * Only for serialization testes in {@link SingletonSerializedTest}
     * */
    private Object readResolve() {
        return SingletonHelper.INSTANCE;
    }
}
